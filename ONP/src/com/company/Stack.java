package com.company;
public class Stack
{
    String[] S = new String[100];
    int pozycja = -1;

    boolean dodaj(String w)
    {
        if(pozycja < 99)
        {
            S[++pozycja] = w;
            return true;
        }
        return false;
    }

    String wez()
    {
        if (pozycja >= 0)
            return S[pozycja];
        else
            return "";
    }

    boolean usun()
    {
        if(pozycja >= 0)
        {
            pozycja--;
            return true;
        }
        return false;
    }

    int size()
    {
        return pozycja + 1;
    }
}