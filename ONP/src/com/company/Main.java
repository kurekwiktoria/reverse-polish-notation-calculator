package com.company;

public class Main {

    public static void main(String[] args)
    {
        String[] ONP = new String[args.length];
        Stack ZNAK = new Stack();

        int j = 0;
        for (String rownania: args)
        {
            System.out.print(args[j]+ " ");
            ONP[j] = "";
            if(rownania.endsWith("="))
            {
                for(int c = 0; c < rownania.length(); ++c)
                {
                    if(rownania.charAt(c) >= '0' && rownania.charAt(c) <= '9' || rownania.charAt(c) == '.')
                    {
                        ONP[j] += rownania.charAt(c);
                        if(!(rownania.charAt(c + 1) >= '0' && rownania.charAt(c + 1) <= '9') && rownania.charAt(c + 1) != '.')
                            ONP[j] += " ";
                    }
                    else switch (rownania.charAt(c)) {

                        case '-':
                        case '+':

                            while (ZNAK.size() > 0 && !ZNAK.wez().equals("(")) {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }

                            ZNAK.dodaj(rownania.charAt(c) + "");
                            break;

                        case '*':
                        case '/':

                            while (ZNAK.size() > 0 && !ZNAK.wez().equals("(") && !ZNAK.wez().equals("+") && !ZNAK.wez().equals("-"))
                            {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }

                            ZNAK.dodaj(rownania.charAt(c) + "");
                            break;


                        case '^':
                            while (ZNAK.size() > 0 && !ZNAK.wez().equals("(") && !ZNAK.wez().equals("+") 
                                    && !ZNAK.wez().equals("-") && !ZNAK.wez().equals("*") && !ZNAK.wez().equals("/")) {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }

                            ZNAK.dodaj(rownania.charAt(c) + "");
                            break;

                        case '%':
                            while (ZNAK.size() > 0 && !ZNAK.wez().equals("(") && !ZNAK.wez().equals("+")
                                    && !ZNAK.wez().equals("-") && !ZNAK.wez().equals("*") && !ZNAK.wez().equals("/")
                                    && !ZNAK.wez().equals("^")) {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }

                            ZNAK.dodaj(rownania.charAt(c) + "");
                            break;

                        case 'V':
                            while (ZNAK.size() > 0 && !ZNAK.wez().equals("(") && !ZNAK.wez().equals("+")
                                    && !ZNAK.wez().equals("-") && !ZNAK.wez().equals("*") && !ZNAK.wez().equals("/")
                                    && !ZNAK.wez().equals("^") && !ZNAK.wez().equals("%")) {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }

                            ZNAK.dodaj(rownania.charAt(c) + "");
                            break;

                        case '(':
                            ZNAK.dodaj(rownania.charAt(c) + "");
                            break;
                        case ')':
                            while (ZNAK.size() > 0 && !ZNAK.wez().equals("(")) {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }
                            ZNAK.usun();
                            break;

                        case '=':
                            while (ZNAK.size() > 0) {
                                ONP[j] += ZNAK.wez() + " ";
                                ZNAK.usun();
                            }
                            break;


                        default:
                            break;
                    }
                }

            }
            else {
                System.out.print("Wprowadzone rownanie jest nieprawidlowe! Brakuje znaku ");
            }
            System.out.print(ONP[j]); System.out.print("= ");


            // Wyliczenie wartości:
            String[] wartosci = ONP[j].split(" ");
            Stack ZNAKw = new Stack();
            for (String element: wartosci)
            {
                if(!element.equals("-") && !element.equals("+") && !element.equals("/") && !element.equals("*")
                        && !element.equals("^")&& !element.equals("%")&& !element.equals("V"))
                {
                    ZNAKw.dodaj(element);
                }
                else if(ZNAKw.size() >= 2)
                {
                    Double y = Double.parseDouble(ZNAKw.wez());
                    ZNAKw.usun();
                    Double x = Double.parseDouble(ZNAKw.wez());
                    ZNAKw.usun();

                    switch (element)
                    {
                        case "+":
                            ZNAKw.dodaj((x + y) + "");
                            break;
                        case "-":
                            ZNAKw.dodaj((x - y) + "");
                            break;
                        case "*":
                            ZNAKw.dodaj((x * y) + "");
                            break;
                        case "/":
                            if(x != 0)
                                ZNAKw.dodaj((x / y) + "");
                            break;
                        case "^":
                            ZNAKw.dodaj((Math.pow(x, y)) + "");
                            break;
                        case "%":
                            ZNAKw.dodaj((x % y) + "");
                            break;
                        case "V":
                            ZNAKw.dodaj((Math.pow(x, (1/y))) + "");
                            break;
                        default:
                            break;
                    }

                }
                else
                    System.out.println("Ups! Cos poszlo nie tak...");
            }
            System.out.println(ZNAKw.wez());
            j++;
        }
    }
}